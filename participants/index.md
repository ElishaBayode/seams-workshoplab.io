---
layout: people
permalink: /participants/
---
{% assign people = site.data.jan2020 %}{% if people.size > 0 %}{% assign people = people | sort %}{% for p in people %}{% assign part = p[1] %}{% include faculty.md profile=part path='jan2020/' %}
{% endfor %}{% else %}No participants submitted their profile 2020...yet!{% endif %}

***

See past participants: [2018](dec2018/)
