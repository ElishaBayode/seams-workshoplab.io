{%- capture nameinfo -%}![{{ include.profile.fullname }}]({{ include.path | default: '' }}{{ include.profile.img }} "{{ include.profile.fullname }}"){% endcapture -%}
- {% if include.profile.status == 'complete' %}[{{ nameinfo }}]({% assign cert = include.profile.img | split: '.' %}{{ cert[0] }}.pdf){% else %}{{ nameinfo }}{% endif %}
  * {{ include.profile.fullname }}{% if include.profile.goby %} (_{{ include.profile.goby }}_){% endif %}{% if include.profile.links %}
  * {% for lk in include.profile.links %}[{{ lk.title }}]({{ lk.url }}){:target="_blank"}{% if forloop.last == false %}, {% endif %}{% endfor %}{% endif %}
{% for aff in include.profile.affiliation %}  * {{ aff.org }}{% if aff.position %}, _{{ aff.position }}_{% endif %}
{% endfor %}