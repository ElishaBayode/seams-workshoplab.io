---
slug: design
title: Project Design & Planning
---

# Stage 1: Initial Challenge

Form into groups of 2.  You are assigned to manage profile pictures for this SEAMS workshop, for use on the website.  You have some, but not all of the pictures.  Some of the pictures are old.  They are in several different formats, are different sizes, and so on.  Generally, a mess.

 - Write down the requirements for this task (remember the requirements exercise from this morning).  The description above may be insufficient, so use your imaginations or ask the faculty.
 - Draw a design diagram for a way to satisfy (programmatically) those requirements
 - Translate that diagram into high-level pseudo-code of the process

# Step 2: Comparative evaluation

Pair up with another team and compare your requirements lists and designs.    

 - Take the first 10 minutes to make sure that each team understands the other team's design.  
 - What differences did you get in requirements lists?  the diagrams? the pseudo code?  How do they relate?
 - Now, how do you evaluate these designs?  How do you probe the strengths and weaknesses of a design?  Write down a few questions or criteria ?

# Step 3: Integration challenge

Software products often operate in an interacting world of data and services.  Imagine that your profile picture system must interact with other data and services in the following ways: 
 - other tools will request full-size or thumbnail images from your system
 - display links to google scholar profile, LinkedIn, Orcid
 - get contact information from an internal personnel system

Now, extend the requirements you previously identified with any additions.  Note: the description above may be incomplete.  Use your imagination or ask a faculty member.
 - Draw a new design diagram to satisfy the new requirements, but highlight the pieces that are preserved from your original diagram
 - Similarly, update your pseudo code, again noting what was preserved from your original process

# Step 4: Final evaluation

If time allows, follow the same steps above. 
